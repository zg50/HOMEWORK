package com.company;

public class Yamaha extends Motorbike {
    public Yamaha(String type, String price) {
        this.type = type;
        this.price = price;
    }

    public void changeColor() {
        System.out.println("Yamaha : color remains the same");
    }

    public void speedUp() {//override the method
        System.out.println("Yamaha is speeding up");
    }

    public void decelerate() {
        System.out.println("Yamaha is decelerating");
    }

    public void left() {
        System.out.println("Yamaha is turning left");
    }

    public void right() {
        System.out.println("Yamaha is turning right");
    }
}
