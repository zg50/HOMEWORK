package com.company;

public class Harley extends Motorbike {
    // price and type inherit from Vehicles
    public Harley (String type, String price) {
        this.price = price;
        this.type = type;
    }

//    public void speedUp() {
//        System.out.println("Harley is speeding up");
//    }
    public void changeColor() {
        System.out.println("Harley changes color from black to red");
    }

    public void decelerate() {
        System.out.println("Harley is decelerating");
    }

    public void left() {
        System.out.println("Harley is turning left");
    }

    public void right() {
        System.out.println("Harley is turning right");
    }
}
