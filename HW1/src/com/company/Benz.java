package com.company;

public class Benz extends Car {
    public Benz (String type, String price) {
        this.type = type;
        this.price = price;
    }

    public void changeColor() {
        System.out.println("Benz changes color from green to white");
    }

    public void speedUp() {
        System.out.println("Benz is speeding up");
    }

    public void decelerate() {
        System.out.println("Benz is decelerating");
    }

    public void left() {
        System.out.println("Benz is turning left");
    }

    public void right() {
        System.out.println("Benz is turning right");
    }
}
