package com.company;

public abstract class Motorbike extends Vehicles {// inherts from Vehicles, does not implement, so it is abstract
    public void speedUp() {// implements speedUp
        System.out.println("Motorbike is speeding up");
    }
}
