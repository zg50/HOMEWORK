package com.company;

public class BMW extends Car {
    public BMW(String type, String price) {
        this.type = type;
        this.price = price;
    }

    public void changeColor() {
        System.out.println("BMW Changes color from blue to black");
    }

    public void speedUp() {
        System.out.println("BMW is speeding up");
    }

    public void decelerate() {
        System.out.println("BMW is decelerating");
    }

    public void left() {
        System.out.println("BMW is turning left");
    }

    public void right() {
        System.out.println("BMW is turning right");
    }
}
