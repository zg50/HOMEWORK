package com.company;

public class Main {

    public static void main(String[] args) {
	  // write your code here
       Vehicles[] vehicles = new Vehicles[4];
       vehicles[0] = new Benz("C400", "$10000");
       vehicles[1] = new BMW("X5", "$15000");
       vehicles[2] = new Harley("750", "$8000");
       vehicles[3] = new Yamaha("classic", "$7000");

       for (int i = 0; i < 4; i++) {
           System.out.println("We are testing " + vehicles[i].type + " the price of which is " + vehicles[i].price);
           vehicles[i].changeColor();
           vehicles[i].speedUp();
           vehicles[i].decelerate();
           vehicles[i].left();
           vehicles[i].right();
       }
    }
}
