package com.company;

public abstract class Vehicles implements Color {//there is abstract method, so the class is abstract
    protected String type;
    protected String price;
    public abstract void speedUp();
    public abstract void decelerate();
    public abstract void left();
    public abstract void right();
}
